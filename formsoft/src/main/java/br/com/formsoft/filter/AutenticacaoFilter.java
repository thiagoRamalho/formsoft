package br.com.formsoft.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import br.com.formsoft.controller.SessaoUsuario;

/**
 * Filter responsavel por verificar se o usuario esta logado
 * antes de acessar recursos da aplicacao
 * 
 * 
 * @author Thiago Ramalho
 *
 */
@WebFilter("*.xhtml")
public class AutenticacaoFilter implements Filter {
	
	@Inject
	private SessaoUsuario sessaoUsuario;
	
	private Logger logger = Logger.getLogger(AutenticacaoFilter.class);

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException {

		HttpServletRequest req  = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		String requestURI = req.getRequestURI();
		
		logger.trace("URI interceptada: "+requestURI);
		
		//se for requisicao de elementos como o css mantem a requisicao
		if(requestURI.contains("resource")){
			logger.trace("requisicao de resources, continua");
			chain.doFilter(request, response);
			return;
		}
		
		//se esta reiniciando a aplicacao, continua
		if(requestURI.contains("iniciar.xhtml")){
			logger.trace("reiniciando a aplicacao");
			chain.doFilter(request, response);
			return;
		}
		
		//se nao esta logado e esta tentando acessar uma pagina diferente da de login
		if(!this.sessaoUsuario.isLogado() && !this.isPaginaLogin(requestURI)){

			logger.trace("nao esta logado e tentando acesso a paginas restritas, redirecionando");
			
			//forca a pagina de login
			res.sendRedirect(req.getContextPath() + "/index.xhtml");
			return;
		}
		
		//se esta logado e esta tentado acessar a pagina de login
		if(this.sessaoUsuario.isLogado() && this.isPaginaLogin(requestURI)){

			logger.trace("esta logado e tentando acesso a pagina de login, redirecionando");
			
			//forca a pagina de produtos
			res.sendRedirect(req.getContextPath() + "/listarprodutos.xhtml");

			return;
		}
		
		logger.trace("sem restricoes, continua requisicao");
		
		//neste ponto esta logado e esta acessando as paginas restritas
		//ou nao esta logado e esta acessando o index, continua a requisicao
		chain.doFilter(request, response);
	}

	private boolean isPaginaLogin(String url){
		return url.endsWith("/index.xhtml") || url.endsWith("/");
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
