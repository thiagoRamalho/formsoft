package br.com.formsoft.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Usuario implements Cloneable{

	@Id @GeneratedValue (strategy = GenerationType.SEQUENCE)
	private Long id;
	private String nick;
	private String senha;
	
	
	public Usuario(){}
	
	private Usuario(Long id, String nick, String senha) {
		super();
		this.id = id;
		this.nick = nick;
		this.senha = senha;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nick=" + nick + ", senha=" + senha
				+ "]";
	}
	
	@Override
	public Usuario clone() {
		return new Usuario(id, nick, senha);
	}
}
