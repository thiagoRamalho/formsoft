package br.com.formsoft.modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
public class Pedido {

	@Id @GeneratedValue (strategy = GenerationType.SEQUENCE)
	private Long id;

	private BigDecimal valorTotal = new BigDecimal("0");
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;
	
	@OneToMany(mappedBy="pedido", cascade=CascadeType.ALL)
	private Collection<Item> itens = new ArrayList<Item>();
	
	//utilizamos o mapa para otimizar o processo de 
	//agrupamento de itens
	@Transient
	private Map<Long, Item> itensAuxiliar = new HashMap<Long, Item>();
	
	public void setData(Calendar data) {
		this.data = data;
	}
	
	public Calendar getData() {
		return data;
	}
	
	public Long getId() {
		return id;
	}
	
	public void adicionar(Produto produto){

		Item item = new Item(produto,this);

		//verifica se ja adicionou esse item
		if(itensAuxiliar.containsKey(produto.getId())){

			//se sim entao devemos agrupa-los
			item = itensAuxiliar.get(produto.getId());
			
			//removemos o valor do item do total do pedido
			//para posteriormente readiciona-lo com a quantidade
			//atualizada
			this.calcularPrecoTotal(item.getPrecoTotal().negate());

			//guarda a quantidade antes de renovar o objeto
			int quantidadeAtual = item.getQuantidade();
			
			//para garantir que o proco mais recente sera o utilizado
			item = new Item(produto, this);
			
			//incrementa o item existente para o calculo do
			//valor total ficar correto
			item.setQuantidade(++quantidadeAtual);
		} 
			
		this.calcularPrecoTotal(item.getPrecoTotal());
		this.itensAuxiliar.put(produto.getId(), item);
	}

	private void calcularPrecoTotal(BigDecimal valor) {
		this.valorTotal = this.valorTotal.add(valor);
	}

	public void remover(Produto p) {
		
		if(this.itensAuxiliar.containsKey(p.getId())){
			
			Item item = this.itensAuxiliar.remove(p.getId());

			//remove do total do pedido o valor do item
			this.calcularPrecoTotal(item.getPrecoTotal().negate());
			
			if(this.itensAuxiliar.isEmpty()){
				System.out.println("lista esta vazia");
			}
		}

		this.itens = new ArrayList<Item>(itensAuxiliar.values());
	}
	
	public void limpar(){
		itensAuxiliar.clear();
	}
	
	public Collection<Item> getItens() {
		
		if(!itensAuxiliar.isEmpty()){
			this.itens = new ArrayList<Item>(itensAuxiliar.values());
		} 		
		return itens;
	}

	public BigDecimal getValorTotal() {
		return this.valorTotal;
	}

	@Override
	public String toString() {
		return "Pedido [id=" + id + ", valorTotal=" + valorTotal + ", data="
				+ data.getTime().toString() + "]";
	}
}
