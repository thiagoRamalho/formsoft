package br.com.formsoft.modelo;

import java.math.BigDecimal;

/**
 * Entidade utilizada apenas para fornecer os dados
 * consolidados para o relatorio
 * 
 * @author Thiago Ramalho
 *
 */
public class ItemAgrupado {

	private Long id;
	private String nome;
	private long quantidadeVendida;
	private BigDecimal valorVendido;

	public ItemAgrupado(Long id, BigDecimal valorVendido, long quantidadeVendida, String nome) {
		super();
		this.id = id;
		this.nome = nome;
		this.quantidadeVendida = quantidadeVendida;
		this.valorVendido = valorVendido;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public long getQuantidadeVendida() {
		return quantidadeVendida;
	}

	public BigDecimal getValorVendido() {
		return valorVendido;
	}
}
