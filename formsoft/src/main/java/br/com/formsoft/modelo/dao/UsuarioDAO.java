package br.com.formsoft.modelo.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.formsoft.modelo.Usuario;

public class UsuarioDAO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	EntityManager entityManager;

	public Usuario buscarPorNickSenha(String nick, String senha){
		
		Usuario usuario = null;
		
		try{
			
			String hql = " from Usuario u where u.nick = :pNick and u.senha = :pSenha";

			TypedQuery<Usuario> query = this.entityManager.createQuery(hql, Usuario.class);

			query.setParameter("pNick", nick);
			query.setParameter("pSenha", senha);

			usuario = query.getSingleResult();

		}catch(NoResultException e){
			//captura a excecao que sera lancada caso nao possua registros 
			//cadastrados
		}

		return usuario;
	}

	public void removerTodos() {
		String hql = " delete from Usuario";
		this.entityManager.createQuery(hql).executeUpdate();
	}

	public void salvar(Usuario usuario) {
		this.entityManager.persist(usuario);
	}

}
