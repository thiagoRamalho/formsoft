package br.com.formsoft.modelo;

/**
 * Interface de marcacao para as entidades, apenas para permitir
 * que seja enviado lista de objetos para o relatorio sem termos
 * que trata-los como Object no @GeradorRelatorio 
 * 
 * @author Thiago Ramalho
 *
 */
public interface Entidade {

}
