package br.com.formsoft.modelo.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.formsoft.modelo.Pedido;

public class PedidoDAO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	EntityManager entityManager;


	public void removerTodos() {
		String hql = " delete from Pedido";
		this.entityManager.createQuery(hql).executeUpdate();
	}

	public void salvar(Pedido pedido) {
		this.entityManager.merge(pedido);
	}

	public List<Pedido> buscarTodos() {
		
		String hql = " from Pedido p ORDER BY p.data DESC ";

		TypedQuery<Pedido> query = this.entityManager.createQuery(hql, Pedido.class);

		List<Pedido> resultList = query.getResultList();
		
		return resultList;
	}
	
	public List<Pedido> buscarComItens() {
		
		List<Pedido> resultList = new ArrayList<Pedido>();
		
		try{
			
			String hql = "SELECT DISTINCT p from Pedido AS p join fetch p.itens ORDER BY p.data DESC ";
			
			TypedQuery<Pedido> query = this.entityManager.createQuery(hql, Pedido.class);
			
			resultList = query.getResultList();

		} catch(NoResultException e){}
		
		return resultList;
	}


	public Pedido buscar(Long id) {
		return this.entityManager.find(Pedido.class, id);
	}

}
