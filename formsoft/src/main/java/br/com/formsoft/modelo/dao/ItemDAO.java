package br.com.formsoft.modelo.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.formsoft.modelo.ItemAgrupado;

public class ItemDAO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	EntityManager entityManager;


	public void removerTodos() {
		String hql = " delete from Item";
		this.entityManager.createQuery(hql).executeUpdate();
	}
	
	/**
	 * Retorna lista de itens agrupados por quantidade e
	 * valor vendido, alem do nome do item
	 * 
	 * @return
	 */
	public List<ItemAgrupado> buscarItensAgrupados(){
			
		List<ItemAgrupado> lista = new ArrayList<ItemAgrupado>();
		
		try{
			
			String jpql = "Select NEW br.com.formsoft.modelo.ItemAgrupado(i.produto.id, sum(i.precoTotal), sum(i.quantidade), " +
					      "i.produto.nome) from Item i " +
					      "group by i.produto.id, i.produto.nome ORDER BY i.produto.nome";
			
			TypedQuery<ItemAgrupado> query = this.entityManager.createQuery(jpql, ItemAgrupado.class);
	
			lista = query.getResultList();

		} catch(NoResultException e){}
		
		return lista;
	}

}
