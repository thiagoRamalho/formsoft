package br.com.formsoft.modelo.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.formsoft.modelo.Produto;

public class ProdutoDAO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	EntityManager entityManager;

	public List<Produto> buscarPorNome(String nome){
		
		List<Produto> produtos = new ArrayList<Produto>();
		
		try{
			
			String hql = " from Produto p where p.nome = :pNome";

			TypedQuery<Produto> query = this.entityManager.createQuery(hql, Produto.class);

			query.setParameter("pNome", nome);

			produtos = query.getResultList();

		}catch(NoResultException e){
			//captura a excecao que sera lancada caso nao possua registros 
			//cadastrados
		}

		return produtos;
	}
	
	public List<Produto> buscarProdutosAbaixoQuantidadeMinima(){
		
		List<Produto> produtos = new ArrayList<Produto>();
		
		try{
			
			String hql = " from Produto p where p.quantidade < p.quantidadeMinima";

			TypedQuery<Produto> query = this.entityManager.createQuery(hql, Produto.class);

			produtos = query.getResultList();

		}catch(NoResultException e){
			//captura a excecao que sera lancada caso nao possua registros 
			//cadastrados
		}

		return produtos;
	}


	public void removerTodos() {
		String hql = " delete from Produto";
		this.entityManager.createQuery(hql).executeUpdate();
	}

	public void salvar(Produto produto) {
		this.entityManager.merge(produto);
	}

	public List<Produto> buscarTodos() {
		
		String hql = " from Produto p ORDER BY p.nome ASC";

		TypedQuery<Produto> query = this.entityManager.createQuery(hql, Produto.class);

		List<Produto> resultList = query.getResultList();
		
		return resultList;
	}

	public Produto buscar(Long id) {
		return this.entityManager.find(Produto.class, id);
	}
}
