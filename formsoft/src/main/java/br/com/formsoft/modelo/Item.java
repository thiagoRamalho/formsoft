package br.com.formsoft.modelo;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Classe que funciona como elemento de ligacao entre 
 * pedido e produto
 * 
 * @author Thiago Ramalho
 *
 */
@Entity
public class Item {

	@Id @GeneratedValue (strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private int quantidade;
	
	private BigDecimal precoUnitario;

	private BigDecimal precoTotal;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	private Produto produto;
	
	@ManyToOne
	private Pedido pedido;
	
	//utilizado pelo ORM
	Item(){}
	
	public Item(Produto produto, Pedido pedido){
		this.quantidade = 1;
		this.pedido = pedido;
		this.produto = produto;
		this.precoUnitario = this.produto.getPreco();
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return this.produto.getNome();
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getPrecoUnitario() {
		return this.precoUnitario;
	}

	public BigDecimal getPrecoTotal(){
		this.precoTotal = this.precoUnitario.multiply(new BigDecimal(quantidade));
		return this.precoTotal;
	}

	public Produto getProduto() {
		return this.produto;
	}
	
	@Override
	public String toString() {
		return "Item [getId()=" + getId() + ", getNome()=" + getNome()
				+ ", getQuantidade()=" + getQuantidade()
				+ ", getPrecoUnitario()=" + getPrecoUnitario()
				+ ", getPrecoTotal()=" + getPrecoTotal() + "]";
	}


}
