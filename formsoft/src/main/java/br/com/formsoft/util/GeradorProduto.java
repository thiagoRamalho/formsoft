package br.com.formsoft.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.formsoft.modelo.Produto;

public class GeradorProduto {

	public Produto criarProduto() {
		return this.criarProdutoComIdENome(new Long(1), "nome");
	}

	public Produto criarProdutoComIdENome(Long id, String nome) {

		Produto produto = new Produto();
		produto.setId(new Long(1));
		produto.setNome("nome");
		produto.setPreco(BigDecimal.ONE);
		produto.setQuantidade(10);
		produto.setQuantidadeMinima(5);

		return produto;
	}


	public List<Produto> criarProdutos(int qtd){

		List<Produto> produtos = new ArrayList<Produto>();

		for(int i = 0; i < qtd; i++){

			Produto p = new Produto();

			String nome =  "Produto " + (i < 10 ? "0" : "") + i;

			p.setNome(nome);
			p.setPreco(new BigDecimal(1+i));
			p.setQuantidade(1 + i);
			p.setQuantidadeMinima(10 + i);

			produtos.add(p);
		}

		return produtos;
	}

}