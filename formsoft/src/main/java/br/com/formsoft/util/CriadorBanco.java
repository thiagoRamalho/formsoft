package br.com.formsoft.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.formsoft.modelo.Usuario;

/**
 * Classe criada apenas para testar a configuracao de conexao do persistenceUnit
 * @author Thiago Ramalho
 *
 */
public class CriadorBanco {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		Usuario u = new Usuario();
		
		u.setNick("nick");
		u.setSenha("senha");
		
		em.persist(u);
		
		em.getTransaction().commit();
		
		em.close();
		emf.close();
		
	}
}
