package br.com.formsoft.util;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

public class GeradorRelatorio{

	private String caminhoRelatorio;

	private Map<String,Object> params;

	private Collection<?> dados;
	
	public GeradorRelatorio(String caminhoRelatorio, Map<String, Object> params, Collection<?> dados) {
		super();
		this.caminhoRelatorio = caminhoRelatorio;
		this.params = params;
		this.dados = dados;
	}


	/**
	 * Retorna inputStream com o fluxo de dados do relatorio
	 * 
	 * @return
	 */
	public ByteArrayOutputStream geraPDFParaInputStream(){

		ByteArrayOutputStream output = new ByteArrayOutputStream();

		try {

			JRBeanCollectionDataSource datasource = new JRBeanCollectionDataSource(dados);

			JasperPrint print = JasperFillManager.fillReport(this.caminhoRelatorio, params, datasource);
			JRExporter exporter = null;

			exporter = new JRPdfExporter();

			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, output);
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
			exporter.exportReport();
			
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}

		return output;
	}

}