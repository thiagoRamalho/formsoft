package br.com.formsoft.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
/**
* Classe que encapsula a inclusão de mensagens
*
*/
public class ContextoFaces implements FacesUtil {

	@Override
	public void adicionarMensagem(String msg, Severity severity) {
		FacesMessage facesMessage = new FacesMessage(msg);
		facesMessage.setSeverity(severity);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
	}
}
