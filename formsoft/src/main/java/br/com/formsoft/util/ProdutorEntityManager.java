package br.com.formsoft.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;


/**
 * Classe responsavel por criar os entityManagers que serao injetados
 * pelo CDI 
 * @author Thiago Ramalho
 *
 */
public class ProdutorEntityManager {

	private Logger logger = Logger.getLogger(ProdutorEntityManager.class);
	
	/**
	 * Com a anotacao ApplicationScoped a fabrica de entityManagers
	 * so sera criada uma vez ao executar a aplicacao
	 * 
	 * @return
	 */
	@Produces @ApplicationScoped
	public EntityManagerFactory criarFactory(){
		logger.trace("Criando entityManagerFactory");
		return Persistence.createEntityManagerFactory("default");
	}
	
	/**
	 * Abre a conexao a cada requisicao
	 * @param entityManagerFactory
	 * @return
	 */
	@Produces @RequestScoped
	public EntityManager criarEntityManager(EntityManagerFactory entityManagerFactory){
		logger.trace("Criando entityManager");
		return entityManagerFactory.createEntityManager();
	}
	
	/**
	 * Com a anotacao disposes iremos fechar a conexao 
	 * 
	 * @param entityManager
	 */
	public void fecharEntityManager(@Disposes EntityManager entityManager){
		logger.trace("Fechando entityManager");
		entityManager.close();
	}
}
