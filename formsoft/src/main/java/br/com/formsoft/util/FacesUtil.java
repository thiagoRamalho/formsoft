package br.com.formsoft.util;

import javax.faces.application.FacesMessage.Severity;

public interface FacesUtil {

	public abstract void adicionarMensagem(String msg, Severity severity);

}