package br.com.formsoft.interceptador;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;


/**
 * Anotacao criada para delimitar os elementos que devem
 * ser interceptados 
 * 
 * @author Thiago Ramalho
 *
 */
@InterceptorBinding @Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD }) 
public @interface Transacao {
}