package br.com.formsoft.interceptador;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

/**
 * Interceptador criado para implementar o padrao OpenSessionInView
 * desse modo a abertura e fechamento do entityManager sera criada
 * e fechada automaticamente nos metodos que estejam anotados com @Transacao
 * 
 * Dessa forma nao teremos problemas de sessao fechada e consequentemente 
 * erros do ORM
 * 
 * @author Thiago Ramalho
 *
 */
@Interceptor
@Transacao
public class TransacaoInterceptor implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager em;

	private Logger logger = Logger.getLogger(TransacaoInterceptor.class);

	@AroundInvoke
	public Object intercept(InvocationContext context) throws Exception{

		logger.trace("iniciando interceptacao");

		em.getTransaction().begin();
		
		Object resultado = context.proceed();
		
		em.getTransaction().commit();

		logger.trace("finalizando interceptacao");

		return resultado;
	}
}
