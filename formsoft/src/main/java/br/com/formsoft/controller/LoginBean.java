package br.com.formsoft.controller;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.formsoft.modelo.Usuario;
import br.com.formsoft.modelo.dao.UsuarioDAO;
import br.com.formsoft.util.FacesUtil;

/**
 * ManagedBean que controla a autenticacao do usuario
 * 
 * @author Thiago Ramalho
 *
 */
@Named
@RequestScoped
public class LoginBean {

	private Usuario usuario = new Usuario();
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private FacesUtil contextoFaces;
	
	@Inject
	private SessaoUsuario sessaoUsuario;
	
	//necessidade do CDI
	LoginBean(){}
	
	//para facilitar os testes
	public LoginBean(UsuarioDAO usuarioDAO, FacesUtil contextoFaces,
			SessaoUsuario sessaoUsuario) {
		super();
		this.usuarioDAO = usuarioDAO;
		this.contextoFaces = contextoFaces;
		this.sessaoUsuario = sessaoUsuario;
	}

	private Logger logger = Logger.getLogger(LoginBean.class);
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public String logar(){
		
		logger.info("iniciando autenticacao");
		
		Usuario uPesquisa = this.usuarioDAO.buscarPorNickSenha(this.usuario.getNick(), this.usuario.getSenha());
		
		if(uPesquisa == null){
			logger.debug(this.usuario +"nao encontrado");
			this.contextoFaces.adicionarMensagem("Usuário/Senha Inválidos", FacesMessage.SEVERITY_ERROR);
			
			return null;
		}
		
		//para evitar problemas pois o objeto esta managed e o coloremos
		//na sessao sem a senha
		uPesquisa = uPesquisa.clone();
		uPesquisa.setSenha("");
		
		this.sessaoUsuario.adicionarUsuario(uPesquisa);
		
		return "listarprodutos?faces-redirect=true";
	}
	
	public String logout(){
		
		this.sessaoUsuario.logout();
		
		return "index?faces-redirect=true";
	}
}
