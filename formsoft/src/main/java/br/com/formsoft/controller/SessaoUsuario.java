package br.com.formsoft.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.formsoft.modelo.Pedido;
import br.com.formsoft.modelo.Usuario;

@Named
@ApplicationScoped
public class SessaoUsuario {

	private Usuario usuario;
	
	private Pedido pedido;
	
	public void criarPedido(){
		this.pedido = new Pedido();
	}
	
	public Pedido recuperarPedido(){
		
		if(this.pedido == null){
			this.criarPedido();
		}
		
		return this.pedido;
	}
	
	public void adicionarUsuario(Usuario usuario){
		this.usuario = usuario;
	}
	
	public boolean isLogado(){
		return this.usuario != null;
	}

	public void logout() {
		this.criarPedido();
		this.usuario = null;
	}
}
