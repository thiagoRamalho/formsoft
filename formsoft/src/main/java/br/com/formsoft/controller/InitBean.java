package br.com.formsoft.controller;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import br.com.formsoft.interceptador.Transacao;
import br.com.formsoft.modelo.Produto;
import br.com.formsoft.modelo.Usuario;
import br.com.formsoft.modelo.dao.ItemDAO;
import br.com.formsoft.modelo.dao.PedidoDAO;
import br.com.formsoft.modelo.dao.ProdutoDAO;
import br.com.formsoft.modelo.dao.UsuarioDAO;
import br.com.formsoft.util.FacesUtil;
import br.com.formsoft.util.GeradorProduto;

/**
 * Classe criada com o proposito de pre-configurar a aplicacao
 * 
 * @author Thiago Ramalho
 *
 */
@Named
@RequestScoped
public class InitBean {

	private final String OUTCOME_INICIAR = "iniciar?faces-redirect=true";
	
	@Inject
	private UsuarioDAO usuarioDAO;

	@Inject
	private ProdutoDAO produtoDAO;

	@Inject
	private ItemDAO itemDAO;

	@Inject
	private PedidoDAO pedidoDAO;

	@Inject
	private FacesUtil contextoFaces;
	
	@Inject
	private SessaoUsuario sessaoUsuario;
	
	@Inject
	private GeradorProduto geradorProduto; 

	private Logger logger = Logger.getLogger(InitBean.class);

	@Transacao
	public void configurar(){

		logger.trace("Eliminando registros do banco");

		this.usuarioDAO.removerTodos();
		this.itemDAO.removerTodos();
		this.produtoDAO.removerTodos();
		this.pedidoDAO.removerTodos();
		
		this.sessaoUsuario.criarPedido();
		
		Usuario u = new Usuario();

		u.setNick("usuario");
		u.setSenha("senha");

		logger.trace("Cadastrando Usuario Padrao: "+u);

		this.usuarioDAO.salvar(u);

		logger.trace("Cadastrando Produtos");

		this.criarProdutosPadrao();
		
		contextoFaces.adicionarMensagem("Cadastros Padrão Realizados!", FacesMessage.SEVERITY_INFO);
	}

	private void criarProdutosPadrao() {
		
		List<Produto> produtos = this.geradorProduto.criarProdutos(5);
		
		for (Produto produto : produtos) {
			this.produtoDAO.salvar(produto);
		}
	}
	
	public String ver(){
		return OUTCOME_INICIAR;
	}
	
	public void sobre() {  
		logger.trace("sobre");
        RequestContext.getCurrentInstance().openDialog("sobre");  
    } 

}
