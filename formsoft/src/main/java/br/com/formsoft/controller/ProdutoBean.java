package br.com.formsoft.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.formsoft.interceptador.Transacao;
import br.com.formsoft.modelo.Produto;
import br.com.formsoft.modelo.dao.ProdutoDAO;
import br.com.formsoft.util.FacesUtil;

/**
 * Bean responsavel pelas acoes relacionadas ao Produto
 * utilizando scopo View nao perderemos o id do Produto
 * em modo de edicao
 * 
 * @author Thiago Ramalho
 *
 */
@Named
@javax.faces.view.ViewScoped
public class ProdutoBean {

	private final String OUTCOME_LISTAR_PRODUTO = "listarprodutos?faces-redirect=true";
	private final String OUTCOME_SALVAR_PRODUTO = "salvarproduto?faces-redirect=true";

	private Produto produto = new Produto();

	@Inject
	private ProdutoDAO produtoDAO;

	@Inject
	private FacesUtil contextoFaces;

	//necessidade do CDI
	ProdutoBean() {
	}
	
	//para facilitar os testes
	public ProdutoBean(ProdutoDAO produtoDAO, FacesUtil contextoFaces, Produto produto) {
		super();
		this.produtoDAO = produtoDAO;
		this.contextoFaces = contextoFaces;
		this.produto = produto;
	}

	private Logger logger = Logger.getLogger(ProdutoBean.class);

	private List<Produto> listaProdutos;

	private Long idSelecionado;

	public void setIdSelecionado(Long idSelecionado) {
		this.idSelecionado = idSelecionado;
	}

	public Long getIdSelecionado() {
		return idSelecionado;
	}

	public Produto getProduto() {
		return produto;
	}

	public List<Produto> getListaProdutos() {
		return listaProdutos;
	}

	/**
	 * Recupera o id da requisicao e busca o objeto
	 * que sera editado
	 * 
	 */
	public void prepararEdicao(){

		logger.trace("Edicao de Item");

		if(this.idSelecionado != null){

			this.produto = this.produtoDAO.buscar(this.idSelecionado);

			logger.info("Recuperado: "+(this.produto) + " para edicao");
		}
	}

	/**
	 * Realiza gravacao do produto, utilizando a anotacao
	 * o cdi ira commitar a acao
	 * 
	 * @return
	 */
	@Transacao
	public String salvar(){

		String navegacao = OUTCOME_LISTAR_PRODUTO;

		logger.info("Salvando: "+(this.produto));

		if(this.produto.getQuantidade() < 1){
			this.contextoFaces.adicionarMensagem("Quantidade Inválida", FacesMessage.SEVERITY_WARN);
			navegacao = null;
		}

		if(this.produto.getQuantidadeMinima() < 1){
			this.contextoFaces.adicionarMensagem("Quantidade Mínima Inválida", FacesMessage.SEVERITY_WARN);
			navegacao = null;
		}

		if(this.produto.getQuantidadeMinima() > this.produto.getQuantidade()){
			this.contextoFaces.adicionarMensagem("Quantidade deve ser maior que Quantidade Mínima", FacesMessage.SEVERITY_WARN);
			navegacao = null;
		}

		if(this.produto.getPreco().compareTo(new BigDecimal("0.01")) < 0){
			this.contextoFaces.adicionarMensagem("Preço Inválido", FacesMessage.SEVERITY_WARN);
			navegacao = null;
		}

		List<Produto> produtos = this.produtoDAO.buscarPorNome(this.produto.getNome());

		//se nao esta vazio devemos verificar se esta tentando cadastrar produto
		//com nome duplicado
		if(!produtos.isEmpty()){
			Produto prodPesquisa = produtos.get(0);

			//se possui id's diferente entao o nome ja foi cadastrado
			if(!prodPesquisa.getId().equals(this.produto.getId())){
				this.contextoFaces.adicionarMensagem("Nome já cadastrado", FacesMessage.SEVERITY_WARN);
				navegacao = null;
			}
		}

		if(navegacao != null){
			this.produtoDAO.salvar(this.produto);
		}

		return navegacao;
	}

	/**
	 * Método chamado pela view para recuperar lista de produtos
	 * 
	 */
	public void recuperar(){

		this.logger.info("Listando produtos");

		this.listaProdutos = this.produtoDAO.buscarTodos();
		this.logger.trace("total de produtos encontrados "+(this.listaProdutos.size()));
	}

	public String listar(){
		return OUTCOME_LISTAR_PRODUTO;
	}
	
	public String ver(){
		return OUTCOME_SALVAR_PRODUTO;
	}
	
	
}
