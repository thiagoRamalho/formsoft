package br.com.formsoft.controller;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.formsoft.interceptador.Transacao;
import br.com.formsoft.modelo.Item;
import br.com.formsoft.modelo.Pedido;
import br.com.formsoft.modelo.Produto;
import br.com.formsoft.modelo.dao.PedidoDAO;
import br.com.formsoft.modelo.dao.ProdutoDAO;
import br.com.formsoft.util.FacesUtil;

/**
 * Classe responsavel por controlar as iteracoes com o carrinho e 
 * os pedidos
 * 
 * @author Thiago Ramalho
 *
 */
@Named
@ViewScoped
public class CarrinhoBean {

	private final String OUTCOME_CARRINHO = "carrinho?faces-redirect=true";

	@Inject
	private SessaoUsuario sessaoUsuario;
	
	@Inject
	private PedidoDAO pedidoDAO;
	
	@Inject
	private ProdutoDAO produtoDAO;
	
	@Inject
	private FacesUtil contextoFaces;
	
	private Logger logger = Logger.getLogger(CarrinhoBean.class);
	
	//exigencia CDI
	CarrinhoBean() {}
	
	public CarrinhoBean(SessaoUsuario sessaoUsuario, PedidoDAO pedidoDAO,
			ProdutoDAO produtoDAO, FacesUtil contextoFaces) {
		super();
		this.sessaoUsuario = sessaoUsuario;
		this.pedidoDAO = pedidoDAO;
		this.produtoDAO = produtoDAO;
		this.contextoFaces = contextoFaces;
	}



	public void adicionar(Produto produto){
		
		logger.info("Adicionando: "+(produto) + "ao pedido");
		
		Pedido pedido = recuperarPedido();
		pedido.adicionar(produto);
		
		this.contextoFaces.adicionarMensagem((produto.getNome())+" adicionado ao carrinho", FacesMessage.SEVERITY_INFO);
	}
	
	public Collection<Item> getItens(){
		Pedido pedido = recuperarPedido();
		return pedido.getItens();
	}
	
	public BigDecimal getValorTotal() {
		Pedido pedido = recuperarPedido();
		return pedido.getValorTotal();
	}
	
	/**
	 * Remove item do pedido
	 * 
	 * @param item
	 */
	public void remover(Item item){
		logger.info("Removendo: "+(item) + " do carrinho");
		Pedido pedido = this.recuperarPedido();
		pedido.remover(item.getProduto());
		
		logger.info("Itens no pedido: "+pedido.getItens().size());
	}
	
	private Pedido recuperarPedido() {
		Pedido pedido = this.sessaoUsuario.recuperarPedido();
		return pedido;
	}
	
	public boolean isEstaVazio(){
		Pedido pedido = this.recuperarPedido();
		return pedido.getItens().isEmpty();
	}
	
	public String limpar(){
		this.sessaoUsuario.criarPedido();
		return OUTCOME_CARRINHO;
	}
	
	public String ver(){
		return OUTCOME_CARRINHO;
	}
	
	@Transacao
	public String finalizar(){
		
		logger.info("finalizando pedido");
		
		Pedido pedido = this.recuperarPedido();
		
		pedido.setData(Calendar.getInstance());
		
		this.darBaixaProduto(pedido.getItens());
		
		this.pedidoDAO.salvar(pedido);
		
		this.contextoFaces.adicionarMensagem("Pedido Finalizado Com Sucesso!", FacesMessage.SEVERITY_INFO);
		
		this.sessaoUsuario.criarPedido();
		
		return "listarprodutos?faces-redirect=true";
	}

	private void darBaixaProduto(Collection<Item> itens) {
		
		for (Item item : itens) {
			
			Produto produto = item.getProduto();
			
			int quantidadeProduto = produto.getQuantidade();
			
			produto.setQuantidade(quantidadeProduto - item.getQuantidade());
			
			this.produtoDAO.salvar(produto);
		}
	}

}
