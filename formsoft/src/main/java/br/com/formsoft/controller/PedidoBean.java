package br.com.formsoft.controller;

import java.util.Collection;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.formsoft.modelo.Pedido;
import br.com.formsoft.modelo.dao.PedidoDAO;

/**
 * Classe responsavel por controlar as iteracoes com o carrinho e 
 * os pedidos
 * 
 * @author Thiago Ramalho
 *
 */
@Named
@ViewScoped
public class PedidoBean {
	
	private final String OUTCOME_PEDIDO = "pedidosrealizados?faces-redirect=true";
	
	@Inject
	private PedidoDAO pedidoDAO;
	
	private Logger logger = Logger.getLogger(PedidoBean.class);
	
	
	public Collection<Pedido> getPedidos(){
		
		logger.info("buscando pedidos realizados");
		
		return pedidoDAO.buscarTodos();
	}
	
	public String ver(){
		return OUTCOME_PEDIDO;
	}
}
