package br.com.formsoft.controller;

import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import br.com.formsoft.modelo.ItemAgrupado;
import br.com.formsoft.modelo.Pedido;
import br.com.formsoft.modelo.Produto;
import br.com.formsoft.modelo.dao.ItemDAO;
import br.com.formsoft.modelo.dao.PedidoDAO;
import br.com.formsoft.modelo.dao.ProdutoDAO;
import br.com.formsoft.util.FacesUtil;
import br.com.formsoft.util.GeradorRelatorio;

@Named
@RequestScoped
public class RelatorioBean {

	@Inject
	private ProdutoDAO produtoDAO;

	@Inject
	private PedidoDAO pedidoDAO;

	@Inject
	private ItemDAO itemDAO;

	@Inject
	private FacesUtil contextoFaces;

	private Logger logger = Logger.getLogger(RelatorioBean.class);


	/**
	 * Executa logica para geracao de relatorio de vendas realizadas
	 * 
	 * @return
	 */
	public void gerarRelatorioVendasEfetuadas(){

		List<Pedido> dados = this.pedidoDAO.buscarComItens();

		this.gerarRelatorio("vendas_efetuadas", new HashMap<String, Object>(), dados);
	}

	/**
	 * Executa logica para geracao de relatorio de quantitativo/financeiro
	 * produtos
	 * 
	 * @return
	 */
	public void gerarRelatorioQuantitativoFinanceiroProdutos(){

		List<ItemAgrupado> dados = this.itemDAO.buscarItensAgrupados();

		this.gerarRelatorio("produto_quantitativo_financeiro", new HashMap<String, Object>(), dados);
	}



	/**
	 * Executa logica para geracao de relatorio de produtos abaixo do minimo
	 * 
	 * @return
	 */
	public void gerarRelatorioProdutosAbaixoMinimo(){
		
		List<Produto> produtosAbaixoMinimo = this.produtoDAO.buscarProdutosAbaixoQuantidadeMinima();

		this.gerarRelatorio("produtos_minimo", new HashMap<String, Object>(), produtosAbaixoMinimo);
	}

	/**
	 * Executa a logica para geracao do relatorio
	 * 
	 * @param nomeRelatorio
	 * @param parametros
	 * @param dados
	 */
	private void gerarRelatorio(String nomeRelatorio, Map<String, Object> parametros, Collection<?> dados){

		try{
			
			this.logger.info("Iniciando geracao do relatorio");
			URL resource = this.getClass().getClassLoader().getResource(nomeRelatorio+".jasper");

			if(dados.isEmpty()){
				this.logger.trace("Nao Possui dados");

				this.contextoFaces.adicionarMensagem("Não Existem dados para geração do Relatório", FacesMessage.SEVERITY_WARN);
				return;
			}

			this.logger.trace("Possui dados");

			FacesContext facesContext = FacesContext.getCurrentInstance();
			facesContext.responseComplete();
			
			//invoca a classe que encapsula as funcionalidades do jasper
			GeradorRelatorio gerador = new GeradorRelatorio(resource.getPath(), parametros, dados);

			byte[] bytes = gerador.geraPDFParaInputStream().toByteArray();
			
			if (bytes != null && bytes.length > 0) {

				this.logger.trace("Iniciando tratamento para array de bytes");
				
				HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

				response.setContentType("application/pdf");

				response.setHeader("Content-disposition", "inline; filename=\""+nomeRelatorio+".pdf\"");

				response.setContentLength(bytes.length);

				ServletOutputStream outputStream = response.getOutputStream();

				outputStream.write(bytes, 0, bytes.length);

				this.logger.trace("inserindo fluxo de dados no resuponse");
				
				outputStream.flush();

				outputStream.close();
				this.logger.trace("finalizando tratamento para array de bytes");
			}
		}catch(Exception e){
			new RuntimeException(e);
		}
		this.logger.trace("Finalizando geracao do relatorio");
	}

}


