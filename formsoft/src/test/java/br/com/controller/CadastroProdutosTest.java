package br.com.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.FakeFaces;
import br.com.formsoft.controller.ProdutoBean;
import br.com.formsoft.modelo.Produto;
import br.com.formsoft.modelo.dao.ProdutoDAO;
import br.com.formsoft.util.ContextoFaces;
import br.com.formsoft.util.GeradorProduto;


public class CadastroProdutosTest {

	private static final String OUT_COME_LISTAR_PRODUTOS = "listarprodutos?faces-redirect=true";
	@Mock
	private ProdutoDAO produtoDAO;
	
	@Mock
	private ContextoFaces contextoFaces;
	
	private ProdutoBean produtoBean;
	
	private Produto produto;
	private GeradorProduto gerador;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		gerador = new GeradorProduto();
		this.produto = gerador.criarProduto();
		produtoBean = new ProdutoBean(produtoDAO, contextoFaces, this.produto);
	}

	@Test
	public void devePassarPorTodasValidacoes() {
		
		//nao possui produtos com o nome enviado
		when(this.produtoDAO.buscarPorNome(anyString())).thenReturn(anyListOf(Produto.class));
		
		//se retornar o outcome correto significa que nao houve erros
		assertEquals(OUT_COME_LISTAR_PRODUTOS, produtoBean.salvar());
		
		//verifica se chamou metodo de gravacao
		verify(this.produtoDAO).salvar(any(Produto.class));
	}

	@Test
	public void deveGerarErroPossuiNomeJaCadastrado() {
		
		Produto produtoPesquisa = new Produto();
		produtoPesquisa.setId(new Long(2));

		//retorna objeto para simular que encontrou resultado
		when(this.produtoDAO.buscarPorNome(anyString())).thenReturn(Collections.singletonList(produtoPesquisa));
		
		//se retornar o outcome correto significa que nao houve erros
		assertEquals(null, produtoBean.salvar());
		
		//nao deve realizar chamada ao metodo que salva o objeto
		verify(this.produtoDAO, never()).salvar(any(Produto.class));
	}

	@Test
	public void deveGerarErroQuantidadeMenorQueUm() {

		this.produto = gerador.criarProduto();
		produto.setQuantidade(0);
		produto.setQuantidadeMinima(0);
		
		ProdutoBean produtoBean = new ProdutoBean(produtoDAO, new FakeFaces(), this.produto);
		
		//retorna objeto para simular que encontrou resultado
		when(this.produtoDAO.buscarPorNome(anyString())).thenReturn(anyListOf(Produto.class));
		
		//se retornar o outcome correto significa que nao houve erros
		assertEquals(null, produtoBean.salvar());
		
		//nao deve realizar chamada ao metodo que salva o objeto
		verify(this.produtoDAO, never()).salvar(any(Produto.class));
	}

	@Test
	public void deveGerarErroQuantidadeMenorQueMinimo() {

		this.produto = gerador.criarProduto();
		produto.setQuantidade(10);
		produto.setQuantidadeMinima(20);
		
		ProdutoBean produtoBean = new ProdutoBean(produtoDAO, new FakeFaces(), this.produto);
		
		//retorna objeto para simular que encontrou resultado
		when(this.produtoDAO.buscarPorNome(anyString())).thenReturn(anyListOf(Produto.class));
		
		//se retornar o outcome correto significa que nao houve erros
		assertEquals(null, produtoBean.salvar());
		
		//nao deve realizar chamada ao metodo que salva o objeto
		verify(this.produtoDAO, never()).salvar(any(Produto.class));
	}

	@Test
	public void deveGerarErroPrecoInvalido() {

		this.produto = gerador.criarProduto();
		produto.setPreco(new BigDecimal("-1"));
		
		ProdutoBean produtoBean = new ProdutoBean(produtoDAO, new FakeFaces(), this.produto);
		
		//retorna objeto para simular que encontrou resultado
		when(this.produtoDAO.buscarPorNome(anyString())).thenReturn(anyListOf(Produto.class));
		
		//se retornar o outcome correto significa que nao houve erros
		assertEquals(null, produtoBean.salvar());
		
		//nao deve realizar chamada ao metodo que salva o objeto
		verify(this.produtoDAO, never()).salvar(any(Produto.class));
	}

	
	/**
	 * Contexto de edicao
	 */
	@Test
	public void deveExecutarCorretamenteEncontrouOProprioProduto() {
		
		//o id nao importa ja que mockamos o dao
		this.produtoBean.setIdSelecionado(new Long(10));
		
		//retorna objeto para simular que encontrou resultado
		when(this.produtoDAO.buscar(anyLong())).thenReturn(this.produto);
		when(this.produtoDAO.buscarPorNome(anyString())).thenReturn(Collections.singletonList(this.produto));
		
		//ira retornar o proprio objeto criado no before
		this.produtoBean.prepararEdicao();
		
		//se retornar o outcome correto significa que nao houve erros
		assertEquals(OUT_COME_LISTAR_PRODUTOS, produtoBean.salvar());
		
		//deve realizar chamada ao metodo que salva o objeto
		verify(this.produtoDAO).salvar(any(Produto.class));
	}
}
