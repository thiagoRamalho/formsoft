package br.com.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.FakeFaces;
import br.com.formsoft.controller.LoginBean;
import br.com.formsoft.controller.SessaoUsuario;
import br.com.formsoft.modelo.Usuario;
import br.com.formsoft.modelo.dao.UsuarioDAO;
import br.com.formsoft.util.FacesUtil;

public class LoginTest {

	@Mock
	private UsuarioDAO usuarioDAO;
	@Mock
	private FacesUtil contextoFaces;
	@Mock
	private SessaoUsuario sessaoUsuario;
	
	private LoginBean loginBean;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.contextoFaces = new FakeFaces();
		loginBean = new LoginBean(usuarioDAO, contextoFaces, sessaoUsuario);
	}

	@Test
	public void loginInvalido() {
		when(this.usuarioDAO.buscarPorNickSenha(anyString(), anyString())).thenReturn(null);
		//retorna falso para outcome da execucao
		assertEquals(null, this.loginBean.logar());
	}

	@Test
	public void loginValido() {
		when(this.usuarioDAO.buscarPorNickSenha(anyString(), anyString())).thenReturn(new Usuario());
		//retorna falso para outcome da execucao
		assertEquals("listarprodutos?faces-redirect=true", this.loginBean.logar());
	}
}
