package br.com.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import br.com.FakeFaces;
import br.com.formsoft.controller.CarrinhoBean;
import br.com.formsoft.controller.SessaoUsuario;
import br.com.formsoft.modelo.Item;
import br.com.formsoft.modelo.Pedido;
import br.com.formsoft.modelo.Produto;
import br.com.formsoft.modelo.dao.PedidoDAO;
import br.com.formsoft.modelo.dao.ProdutoDAO;
import br.com.formsoft.util.GeradorProduto;


public class CarrinhoTest {

	@Mock
	private PedidoDAO pedidoDAO;
	
	@Mock
	private ProdutoDAO produtoDAO;
	
	private CarrinhoBean carrinhoBean;

	private GeradorProduto gerador;
	
	@Before
	public void setUp() throws Exception {
		gerador = new GeradorProduto();
		this.carrinhoBean = new CarrinhoBean(new SessaoUsuario(), pedidoDAO, produtoDAO, new FakeFaces());
	}

	/**
	 * Como os produtos sao agrupados, sempre que adicionamos o mesmo
	 * item o resultado sera apenas um item com quantidade modificada
	 * 
	 */
	@Test
	public void deveAdicionarProdutoNoCarrinho() {
		this.carrinhoBean.adicionar(gerador.criarProduto());
		this.carrinhoBean.adicionar(gerador.criarProduto());
		this.carrinhoBean.adicionar(gerador.criarProduto());
		
		Produto novoProduto = gerador.criarProduto();
		novoProduto.setId(new Long(999));
		novoProduto.setNome("novo nome");
		
		this.carrinhoBean.adicionar(novoProduto);
		
		assertEquals(2, this.carrinhoBean.getItens().size());
		
		assertEquals(new BigDecimal("4"), this.carrinhoBean.getValorTotal());
	}
	
	@Test
	public void deveRemoverProdutoNoCarrinho() {
		
		this.carrinhoBean.adicionar(gerador.criarProduto());
		
		Produto novoProduto = gerador.criarProduto();
		novoProduto.setId(new Long(999));
		novoProduto.setNome("novo nome");
		
		this.carrinhoBean.adicionar(novoProduto);

		this.carrinhoBean.remover(new Item(gerador.criarProduto(), new Pedido()));
	
		assertFalse(this.carrinhoBean.isEstaVazio());
		
		assertEquals(1, this.carrinhoBean.getItens().size());
		
		assertEquals(new BigDecimal("1"), this.carrinhoBean.getValorTotal());
	}
	@Test
	public void deveRemoverTodosProdutoNoCarrinho() {
		
		Produto p1 = gerador.criarProduto();
		
		this.carrinhoBean.adicionar(p1);
		
		Produto novoProduto = gerador.criarProdutoComIdENome(new Long(990), "novo nome");
		
		this.carrinhoBean.adicionar(novoProduto);

		this.carrinhoBean.remover(new Item(p1, new Pedido()));
		this.carrinhoBean.remover(new Item(novoProduto, new Pedido()));
		
		assertEquals(0, this.carrinhoBean.getItens().size());
	}
	
	@Test
	public void deveLimparCarrinho() {
		
		this.carrinhoBean.adicionar(gerador.criarProduto());

		this.carrinhoBean.limpar();
	
		assertTrue(this.carrinhoBean.isEstaVazio());
		
		assertEquals(BigDecimal.ZERO, this.carrinhoBean.getValorTotal());
	}
}
