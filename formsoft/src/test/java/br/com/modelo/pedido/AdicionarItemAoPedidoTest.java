package br.com.modelo.pedido;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import br.com.formsoft.modelo.Item;
import br.com.formsoft.modelo.Pedido;
import br.com.formsoft.modelo.Produto;

public class AdicionarItemAoPedidoTest {

	private final int NUMERO_ITENS_PEDIDO = 3;
	private final BigDecimal VALOR_TOTAL_PEDIDO  = new BigDecimal("6");
	private Pedido pedido;

	@Before
	public void setUp() throws Exception {
		pedido = new Pedido();
		this.adicionarProdutos(NUMERO_ITENS_PEDIDO);
	}

	@Test
	public void deveAdicionarProdutosAoPedido() {
		assertEquals(NUMERO_ITENS_PEDIDO, this.pedido.getItens().size());
		assertEquals(VALOR_TOTAL_PEDIDO,  this.pedido.getValorTotal());
	}
	
	@Test
	public void deveAgruparProdutosIguais() {
		
		//adiciona novamente, com isso acontecera a repeticao
		//dos itens ja adicionados no @before
		this.adicionarProdutos(NUMERO_ITENS_PEDIDO);
		
		//adiciona novamente
		this.adicionarProdutos(NUMERO_ITENS_PEDIDO);
		
		//numero de itens nao deve ser alterado
		assertEquals(NUMERO_ITENS_PEDIDO,     this.pedido.getItens().size());
		
		//multiplica o valor para confirmar que a soma tambem esta funcionando
		assertEquals(VALOR_TOTAL_PEDIDO.multiply(new BigDecimal("3")),  this.pedido.getValorTotal());
		
		this.exibir(this.pedido.getItens());
	}

	@Test
	public void deveRemoverProduto() {
		
		//adiciona novamente, com isso acontecera a repeticao
		//dos itens ja adicionados no @before
		this.adicionarProdutos(NUMERO_ITENS_PEDIDO);
		
		//numero de itens nao deve ser alterado
		assertEquals(NUMERO_ITENS_PEDIDO,     this.pedido.getItens().size());
		
		Produto p = new Produto();
		p.setId(new Long(3));
		p.setPreco(new BigDecimal("3"));
		
		//removemos o produto 3 
		pedido.remover(p);
		
		//numero de itens deve ser apenas 02 nesse momento
		assertEquals(NUMERO_ITENS_PEDIDO - 1,     this.pedido.getItens().size());
		
		//removemos o produto 03 que foi adicionado duas vezes, portanto
		//possui um valor total de 06, ficando 06 como valor restante
		//ja que o total original seria 12
		assertEquals(new BigDecimal("6"),  this.pedido.getValorTotal());
		
		this.exibir(this.pedido.getItens());
	}


	private void exibir(Collection<Item> itens){
		for (Item item : itens) {
			System.out.println(item);
		}
		
		System.out.println("--------------------------------");
	}
	
	private void adicionarProdutos(int numeroDeProdutos) {

		for(int i = 1; i <= numeroDeProdutos; i++){

			Produto p = new Produto();

			String nome =  "Produto " + i;
			
			p.setId(new Long(i));
			p.setNome(nome);
			p.setPreco(new BigDecimal(i));
			p.setQuantidade(i);
			p.setQuantidadeMinima(i);

			this.pedido.adicionar(p);
		}
	}
}
