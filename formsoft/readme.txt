## RECOMENDACOES ##

Caso seja necessário modificar os projetos *.jrxml
Será necessário criar um jar do pacote modelo e
adicioná-lo ao classpath do IReport

Ao executar a aplicação será necessário clicar no botão
Iniciar a aplicação para que o usuário e senha e outros
cadastros sejam criados

## USO ##
Produto
* Cadastrar: Cadastra novo produto
* Listar: Exibe todos os produtos cadastrados

Pedido
* Ver Carrinho: Exibe os produtos adicionados que
                caso clique em finalizar esses 
                itens serao convertidos em um
                pedido

* Realizados: Exibe a lista de pedidos finalizados

Relatorios
 * Produtos abaixo minimo: Cria relatorio com todos os 
                           produtos que tem sua quantidade
                           menor que a quantidade minima
                           definida (a subtracao e realizada
                           no momento que e feita a finalizacao
                           do pedido na opcao ver carrinho)
                           
* Vendas Efetuadas: Cria relatorio com os pedidos e seus respectivos
                    produtos

* Quantitativo Financeiro Produtos: Cria relatorio com o total de produtos
                                    vendidos agrupados por valor e quantidade   
                                    
Iniciar
* Leva para pagina que apaga os registros  do BD e recria os cadastros padrao

Sobre
* Informacoes sobre as tecnologias utilizadas no projeto

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                                     
